## SEQUENCE DIAGRAM
![](resources/sequence_diagram.png)

## CLASS DIAGRAM
![](resources/class_diagram.png)

## STATE-TRANSITION DIAGRAM
![](resources/state_transition_diagram.png)
