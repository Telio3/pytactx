def eval(agent, neighbour) -> int:
    """ Returns the cost of going to the neighbour """

    # define the weights
    distance_weight = 1
    ammo_weight = 1
    life_weight = 2
    fire_weight = 3

    # define the scale
    distance_scale = 1
    ammo_scale = 0.1
    life_scale = 0.1
    fire_scale = 1

    # calculate the different between the agent and the neighbour
    distance = abs(agent.x - neighbour['x']) + abs(agent.y - neighbour['y'])
    life_difference = neighbour['life'] - agent.vie
    ammo_difference = neighbour['ammo'] - agent.munitions

    # calculate the cost
    cost = distance_weight * distance_scale * distance + \
              ammo_weight * ammo_scale * ammo_difference + \
                life_weight * life_scale * life_difference + \
                    fire_weight * fire_scale * neighbour['fire']

    return cost
