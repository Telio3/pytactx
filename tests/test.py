import unittest

from tools.eval import eval


class AgentMock:
    def __init__(self, vie, munitions, x, y):
        self.vie = vie
        self.munitions = munitions
        self.x = x
        self.y = y


class TestEval(unittest.TestCase):
    def test_eval_between_two_agents(self):
        agent = AgentMock(10, 10, 2, 3)
        neighbour = {'life': 20, 'fire': 2, 'ammo': 5, 'x': 3, 'y': 4}
        result = eval(agent, neighbour)
        self.assertEqual(result, 9.5)


if __name__ == '__main__':
    unittest.main()
