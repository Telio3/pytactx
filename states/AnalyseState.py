import math

from states.BaseState import BaseState
from states.enum.StateEnum import StateEnum
from tools.eval import eval
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from states.StateMachineAgent import StateMachineAgent


class AnalyseState(BaseState):
    """ Analyse the costs for each neighbour and choose the best one to kill """

    def __init__(self, agent: 'StateMachineAgent'):
        super().__init__(agent, (0, 255, 0))

    def get_nearest_neighbour(self, neighbours) -> str:
        """ Returns the neighbour that is the closest to the agent """

        nearest_neighbour = None
        nearest_distance = float('inf')

        # get the neighbour that is the closest to the agent
        for name, neighbour in neighbours.items():
            distance = math.sqrt((self._agent.x - neighbour['x']) ** 2 + (self._agent.y - neighbour['y']) ** 2)
            if distance < nearest_distance:
                nearest_distance = distance
                nearest_neighbour = name

        return nearest_neighbour

    def choose_target(self, neighbours) -> str:
        """ Returns the neighbour that is the closest to the agent """

        # display the neighbours
        print(list(neighbours.keys()))

        # calculate the costs for each neighbour
        costs = {}
        for name, neighbour in neighbours.items():
            costs[name] = eval(self._agent, neighbour)
            print(name, costs[name])

        # get the neighbour with the lowest cost
        min_cost = min(costs.values())
        min_cost_neighbours = [name for name, cost in costs.items() if cost == min_cost]

        # if there is more than one neighbour with the lowest cost, choose the closest one
        if len(min_cost_neighbours) == 1:
            return min_cost_neighbours[0]

        return self.get_nearest_neighbour({name: neighbours[name] for name in min_cost_neighbours})

    def onHandle(self):
        """ Analyse the costs for each neighbour and choose the best one to kill """
        
        print("AnalyseState")

        # stop shooting
        self._agent.tirer(False)

        # turn not to be absent
        self._agent.orienter((self._agent.orientation + 1) % 4)

        # if there is no target, do nothing
        if len(self._agent.voisins) == 0:
            return

        # choose the best neighbour to kill
        target = self.choose_target(self._agent.voisins)
        self._agent.set_target(target)
        self.switch_state(StateEnum.GO_TO_TARGET.value)
