from states.BaseState import BaseState
from states.enum.StateEnum import StateEnum
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from states.StateMachineAgent import StateMachineAgent


class KillState(BaseState):
    """ Kill the enemy """

    def __init__(self, agent: 'StateMachineAgent'):
        super().__init__(agent, (255, 0, 0))

    def target_is_dead(self) -> bool:
        """ Returns true if the target is dead """

        return self._agent.targetName not in self._agent.voisins

    def onHandle(self):
        """ Go to target and kill it """
        
        print("KillState")

        # if the target is dead, switch to analyse state
        if self.target_is_dead() or self._agent.distance == 0:
            self._agent.set_target(None)
            self.switch_state(StateEnum.ANALYSE.value)
        else:
            # otherwise shoot
            self._agent.tirer(True)
