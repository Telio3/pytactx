from states import fsm

from abc import abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from states.StateMachineAgent import StateMachineAgent


class BaseState(fsm.State):
    def __init__(self, agent: 'StateMachineAgent', color):
        self._agent = agent
        self._color = color
        super().__init__()

    def handle(self):
        """ Handle the state """
        r, g, b = self._color
        self._agent.changerCouleur(r, g, b)
        self.onHandle()

    @abstractmethod
    def onHandle(self):
        pass
