from abc import ABC, abstractmethod


class IState(ABC):
    @abstractmethod
    def handle(self):
        pass


class State(IState, ABC):
    def __init__(self):
        self.__context = None

    def setContext(self, context):
        self.__context = context

    def switch_state(self, state: int):
        self.__context.set_actual_state(state)


class StateMachine:
    def __init__(self):
        self.__actual_state = 0
        self.__states = []

    def add_state(self, state: State):
        state.setContext(self)
        self.__states.append(state)

    def handle(self):
        self.__states[self.__actual_state].handle()

    def set_actual_state(self, state: int):
        self.__actual_state = state
