from states import fsm
from pytactx import Agent
from states.AnalyseState import AnalyseState
from states.enum.StateEnum import StateEnum
from states.GoToTarget import GoToTargetState
from states.KillState import KillState


class StateMachineAgent(Agent):
    """ State Machine Agent """
    def __init__(self, myId):
        self.targetName = None
        self.target = None

        # Create the state machine
        self.__state_machine = fsm.StateMachine()

        # Add states to the state machine
        self.__state_machine.add_state(AnalyseState(self))
        self.__state_machine.add_state(GoToTargetState(self))
        self.__state_machine.add_state(KillState(self))

        # Set the initial state
        self.__state_machine.set_actual_state(StateEnum.ANALYSE.value)

        _pwd = input('Enter the password: ')
        super().__init__(id=myId,
                         username="demo",
                         password=_pwd,
                         arena="demo",
                         server="mqtt.jusdeliens.com",
                         prompt=False,
                         verbose=False)

    def quandActualiser(self):
        """ Called when the agent is updated """
        self.__state_machine.handle()

    def set_target(self, targetName):
        """ Set the target of the agent """
        self.targetName = targetName

        if targetName is not None:
            self.target = self.voisins[targetName]
        else:
            self.target = None
