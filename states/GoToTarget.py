from states.BaseState import BaseState
from states.enum.StateEnum import StateEnum
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from states.StateMachineAgent import StateMachineAgent


class GoToTargetState(BaseState):
    """ Go to the target """

    def __init__(self, agent: 'StateMachineAgent'):
        super().__init__(agent, (0, 0, 255))

    def agent_move_next_to_target(self):
        """ Move the agent next to the target """

        if self._agent.x - self._agent.target['x'] < 0:
            self._agent.deplacer(self._agent.x + 1, self._agent.y)
        elif self._agent.x - self._agent.target['x'] > 0:
            self._agent.deplacer(self._agent.x - 1, self._agent.y)
        elif self._agent.y - self._agent.target['y'] < 0:
            self._agent.deplacer(self._agent.x, self._agent.y + 1)
        elif self._agent.y - self._agent.target['y'] > 0:
            self._agent.deplacer(self._agent.x, self._agent.y - 1)

    def onHandle(self):
        """ Go to the target """

        print("GoToTargetState")

        # stop shooting
        self._agent.tirer(False)

        # if the agent is close enough to the target, switch to kill state
        if self._agent.distance != 1:
            # go to target
            x, y = self._agent.x, self._agent.y  # agent position before moving
            self._agent.deplacer(self._agent.target['x'], self._agent.target['y'])

            # if the agent is in adjacent box to the neighbour
            if x == self._agent.x and y == self._agent.y:
                self.agent_move_next_to_target()

            # if the target has moved, go to the new target
            if self._agent.x == self._agent.target['x'] and self._agent.y == self._agent.target['y']:
                self.switch_state(StateEnum.ANALYSE.value)

            # face the neighbour
            if self._agent.x - self._agent.target['x'] < 0:
                self._agent.orienter(0)
            elif self._agent.x - self._agent.target['x'] > 0:
                self._agent.orienter(2)
            elif self._agent.y - self._agent.target['y'] < 0:
                self._agent.orienter(3)
            elif self._agent.y - self._agent.target['y'] > 0:
                self._agent.orienter(1)
        else:
            self.switch_state(StateEnum.KILL.value)
