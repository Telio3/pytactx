from enum import Enum


class StateEnum(Enum):
    """ State Enum """

    ANALYSE = 0
    GO_TO_TARGET = 1
    KILL = 2
